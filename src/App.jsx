import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { I18nProvider } from "./i18n/I18nProvider";
import { MaterialThemeProvider } from "./layout/MaterialThemeProvider";
import { store } from "./redux/Store";
import { PublicRoutes } from "./router/PublicRoutes";
import { ThemeContext } from "./utility/context/ThemeColors";

// Import Core Style
import "@styles/style.scss";

export default function App({ basename }) {
  return (
    <>
      {/* Wrap App with Redux Provirer with store */}
      <Provider store={store}>
        {/* Override `basename` (e.g: `homepage` in `package.json`) */}
        <BrowserRouter basename={basename}>
          {/* Material Theme Config */}
          <MaterialThemeProvider>
            {/* Use Theme core context */}
            <ThemeContext>
              {/* Provide `react-intl` context synchronized with Redux state.  */}
              <I18nProvider>
                {/* Toast Notification Provider */}
                <ToastContainer
                  position="top-left"
                  autoClose={7000}
                  hideProgressBar={false}
                  newestOnTop
                  pauseOnFocusLoss
                  draggable
                  pauseOnHover
                />
                {/* Base Router File */}
                <PublicRoutes />
              </I18nProvider>
            </ThemeContext>
          </MaterialThemeProvider>
        </BrowserRouter>
      </Provider>
    </>
  );
}
