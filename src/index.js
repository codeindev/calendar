import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Y2AI18nProvider } from "./i18n";
import reportWebVitals from "./reportWebVitals";

// Import web fonts
import '@styles/base/typhography/_EnglishFonts.scss'
import '@styles/base/typhography/_persian-fonts.scss'

ReactDOM.render(
  <Y2AI18nProvider>
    <App />
  </Y2AI18nProvider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
