import React, { Suspense } from "react";
import { Redirect, Switch, Route } from "react-router-dom";

import Dashboard from "../view/Dashboard/Dashboard";


export default function ProtectedRoutes() {
  return (
    <Suspense>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/" to="/dashboard" />
        }

        <Route path="/dashboard" component={Dashboard} />
   

      </Switch>
    </Suspense>
  );
}
