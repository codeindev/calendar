import React, { useEffect, useState } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { shallowEqual, useSelector, useDispatch } from "react-redux";

import Layout from "../layout/Layout";
import ProtectedRoutes from "./ProtectedRoutes";
import AuthPage from "../view/Auth/AuthPage";
import { loadUser } from "../redux/actions/auth/loginActions";

export function PublicRoutes() {
  const dispatch = useDispatch();

  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const { isLoginSuccData, isLogOutData, isAuthorized } = useSelector(
    ({ auth }) => ({
      isLoginSuccData: auth.login.isLoginSucc != null,
      isLogOutData: auth.login.isLogOut,
      isAuthorized: auth.login.token != null,
    }),
    shallowEqual
  );

  useEffect(() => {
    if (localStorage.getItem("token") != null) {
      if (isAuthorized) {
        setIsAuthenticated(true);
        dispatch(loadUser());
        dispatch({
          type: "AUTH_SERVER_HAS_SUCC",
        });
      } else {
        dispatch({
          type: "AUTH_SERVER_HAS_ERR",
        });
        // setIsAuthenticated(false);
      }
    }
    if (isLoginSuccData) {
      window.location.reload(true);
    }
    if (isLogOutData != null) {
      window.location.reload(true);
    }
  }, [isLoginSuccData, isLogOutData, dispatch, isAuthorized]);

  return (
    <Switch>
      {!isAuthorized && !isAuthenticated ? (
        /*Render auth page when user at `/auth` and not authorized.*/
        <Route>
          <AuthPage />
        </Route>
      ) : (
        /*Otherwise redirect to root page (`/`)*/
        <Redirect from="/auth" to="/" />
      )}

      {!isAuthorized && !isAuthenticated ? (
        /*Redirect to `/auth` when user is not authorized*/
        <Redirect to="/auth/login" />
      ) : (
        <Layout>
          <ProtectedRoutes />
        </Layout>
      )}

      {/* <Route path="/logout" component={Logout} /> */}
    </Switch>
  );
}
