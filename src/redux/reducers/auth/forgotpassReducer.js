/* eslint-disable */
const initialState = {
  fpStepOne: null,
  userDontExist: null,
  resetPassSuc: null,
  resetPassErr: null,
};

export const forgotPassword = (state = initialState, action) => {
  switch (action.type) {
    case "RESET_PASSWOR_STEP_ONE_SUCCESS":
      return {
        ...state,
        fpStepOne: true,
        userDontExist: null,
      };
    case "RESET_PASSWOR_STEP_ONE_ERR":
      return {
        ...state,
        fpStepOne: false,
        userDontExist: null,
      };
      
      setTimeout(() => {
        return {
          ...state,
          userDontExist: null,
        };
      }, 3000);
    case "USER_DONT_EXIST":
      return {
        ...state,
        userDontExist: true,
      };
      setTimeout(() => {
        return {
          ...state,
          userDontExist: null,
        };
      }, 3000);

    case "RESET_PASSWOR_SUCCESS":
      return {
        ...state,
        resetPassSuc: true,
      };
      setTimeout(() => {
        return {
          ...state,
          resetPassSuc: null,
        };
      }, 3000);
    case "RESET_PASSWOR_FAIL":
      return {
        ...state,
        resetPassErr: true,
      };
      setTimeout(() => {
        return {
          ...state,
          resetPassErr: null,
        };
      }, 3000);

    default:
      return state;
  }
};
