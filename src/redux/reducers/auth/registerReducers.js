/* eslint-disable */
const initialState = {
  registerSucMSG: null,
  firstNameRequiredMSG: null,
  lastNameRequiredMSG: null,
  usernameExistErrMSG: null,
  emailExistErrMSG: null,
  phonenumberExistErrMSG: null,
  passwordCharNumberMSG: null,
  passwordNotMatchMSG: null,
  userAccountActiveMSG: null,
  userAccountActiveERR: null,
};

export const register = (state = initialState, action) => {
  switch (action.type) {
    case "REGISTER_SUCCESS":
      return {
        ...state,
        registerSucMSG: true,
        firstNameRequiredMSG: null,
        lastNameRequiredMSG: null,
        usernameExistErrMSG: null,
        emailExistErrMSG: null,
        phonenumberExistErrMSG: null,
        passwordCharNumberMSG: null,
        passwordNotMatchMSG: null,
        userAccountActiveMSG: null,
        userAccountActiveERR: null,
      };
      setTimeout(() => {
        return {
          ...state,
          registerSucMSG: null,
        };
      }, 3000);
    case "USER_ACCOUNT_ACTIVE_SUC":
      return {
        ...state,
        userAccountActiveMSG: true,
      };
      setTimeout(() => {
        return {
          ...state,
          userAccountActiveMSG: null,
        };
      }, 3000);
    case "USER_ACCOUNT_ACTIVE_ERR":
      return {
        ...state,
        userAccountActiveERR: true,
      };
      setTimeout(() => {
        return {
          ...state,
          userAccountActiveERR: null,
        };
      }, 3000);
    case "PASSWORD_NOT_MATCH":
      return {
        ...state,
        passwordNotMatchMSG: true,
      };
      setTimeout(() => {
        return {
          ...state,
          passwordNotMatchMSG: null,
        };
      }, 3000);
    case "PASSWORD_MUST_8_CHAR":
      return {
        ...state,
        passwordCharNumberMSG: true,
      };
      setTimeout(() => {
        return {
          ...state,
          passwordCharNumberMSG: null,
        };
      }, 3000);
    case "FIRSTNAME_REQUIRED":
      return {
        ...state,
        firstNameRequiredMSG: true,
      };
      setTimeout(() => {
        return {
          ...state,
          firstNameRequiredMSG: null,
        };
      }, 3000);
    case "USERNAME_HAS_EXIST":
      return {
        ...state,
        usernameExistErrMSG: true,
      };
      setTimeout(() => {
        return {
          ...state,
          usernameExistErrMSG: null,
        };
      }, 3000);

    case "EMAIL_HAS_EXIST":
      return {
        ...state,
        emailExistErrMSG: true,
      };
      setTimeout(() => {
        return {
          ...state,
          emailExistErrMSG: null,
        };
      }, 3000);

    case "PHONENUMBER_HAS_EXIST":
      return {
        ...state,
        phonenumberExistErrMSG: true,
      };
      setTimeout(() => {
        return {
          ...state,
          phonenumberExistErrMSG: null,
        };
      }, 3000);

    default:
      return state;
  }
};
