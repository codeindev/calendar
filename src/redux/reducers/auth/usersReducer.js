const initialState = {
  users: [],
  usersCount: 0,
  userTableRowID: "",
  updateUserSUC: null,
  updateUserERR: null,
  createUserSUC: null,
  createUserERR: null,
  deleteUserSUC: null,
  deleteUserERR: null,
  addUserModal : false,
  UserDeleteModal : false
};

export const user = (state = initialState, action) => {
  switch (action.type) {
    case "GET_ALL_USERS_LIST_SUCC":
      return {
        ...state,
        users: action.payload.users,
      };
    case "USERS_COUNT_SUCC":
      return {
        ...state,
        usersCount: action.payload.users_count,
      };
    case "GET_USERS_TABLE_ROW_ID":
      return {
        ...state,
        userTableRowID: action.payload,
      };
    case "SHOW_ADD_USER_MODAL":
      return {
        ...state,
        addUserModal: true,
      };
    case "HIDE_ADD_USER_MODAL":
      return {
        ...state,
        addUserModal: false,
      };
    case "CREATE_USER_SUCC":
      return {
        ...state,
        createUserSUC: true,
        createUserERR: null,
      };
    case "CREATE_USER_ERR":
      return {
        ...state,
        createUserERR: true,
      };
    case "CREATE_USER_SUCC_UNDO":
      return {
        ...state,
        createUserSUC: null,
      };
    case "SHOW_USER_DELETE_MODAL":
      return {
        ...state,
        UserDeleteModal: true,
      };
    case "HIDE_USER_DELETE_MODAL":
      return {
        ...state,
        UserDeleteModal: false,
      };
    case "DELETE_USER_SUCC":
      return {
        ...state,
        deleteUserSUC: true,
        deleteUserERR : null
      };
    case "DELETE_USER_ERR":
      return {
        ...state,
        deleteUserERR: true,
      };
    case "DELETE_USER_SUCC_UNDO":
      return {
        ...state,
        deleteUserSUC: null,
      };

    default:
      return state;
  }
};
