import { API_URL } from "./config";
/* ------------------------ Authentication CRUD URLs ------------------------ */

export const REGISTER_URL = `${API_URL}/api/v1/register`;
export const REGISTER_ACTIVE_URL = `${API_URL}/api/v1/register/activation`;
export const LOGIN_URL = `${API_URL}/api/v1/login`;
export const LOGOUT_URL = `${API_URL}/api/v1/logout`;
export const ME_URL = `${API_URL}/api/v1/profile`;
export const REQUEST_PASSWORD_URL = `${API_URL}/api/v1/forgot_password`;
export const CHANGE_PASSWORD_URL = `${API_URL}/api/v1/profile/password`;
export const TWO_FA_CODE_DISABLE_URL = `${API_URL}/api/v1/profile/2fa/disable`;
export const PAGE_AFTER_LOGIN_URL = `${API_URL}/api/v1/profile/postLoginPage`;
export const GET_USERS_COUNT_URL = `${API_URL}/api/v1/userscount`;


export const GET_ALL_USERS_URL = `${API_URL}/api/v1/users`;
export const CREATE_USER_URL = `${API_URL}/api/v1/create-user`;
export const DELETE_USER_URL = `${API_URL}/api/v1/user`;

export const FORGOT_PASS_URL = `${API_URL}/api/v1/password/create`;
export const RESET_PASS_URL = `${API_URL}/api/v1/password/reset`;

/* ----------------------------- Activity CRUDS ----------------------------- */
export const GET_USER_ACTIVITY = `${API_URL}/api/v1/user-activity`;


/* ------------------------------- TwoFa CRUDS ------------------------------ */

export const GET_QR_CODE_URL = `${API_URL}/api/v1/twofa-qr`;
export const CONFIRM_QR_CODE_URL = `${API_URL}/api/v1/confirm-twofa`;
export const GET_STATUS_QR_CODE_URL = `${API_URL}/api/v1/twofa-status`;
