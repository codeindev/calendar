import axios from "axios";

import * as authCrud from "../../cruds/authCruds";

/* ------------------------------- Login User ------------------------------- */

export const loginAction = ({ email, password }) => (dispatch) => {
  // Headers
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  // Request body
  const body = { email, password };

  axios
    .post(authCrud.LOGIN_URL, body, config)
    .then((res) => {
      dispatch({
        type: "LOGIN_SUCCESS",
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: "LOGIN_FAIL",
        payload: err.data,
      });
      setTimeout(() => {
        dispatch(undoErrMsg());
      }, 3000);
    });
};

/* ------------------------- Check Token & load user ------------------------ */
export const loadUser = () => (dispatch) => {
  // Get token from localstorage
  const token = localStorage.getItem("token");

  // Headers
  const config = {
    headers: {
      "Content-type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  };
  axios
    .get(authCrud.ME_URL, config)
    .then((res) => {
      dispatch({
        type: "USER_LOADED",
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: "AUTH_ERROR",
        payload: err.data,
      });
    });
};

/* ------------------------- Get Users Count ------------------------ */
export const getUsersCount = () => (dispatch) => {
  // Get token from localstorage
  const token = localStorage.getItem("token");

  // Headers
  const config = {
    headers: {
      "Content-type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  };
  axios
    .get(authCrud.GET_USERS_COUNT_URL, config)
    .then((res) => {
      dispatch({
        type: "USERS_COUNT_SUCC",
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: "USERS_COUNT_ERR",
        payload: err.data,
      });
    });
};

/* ----------------------- undo Login Err msg ---------------------- */

export const undoErrMsg = () => (dispatch) => {
  dispatch({
    type: "LOGIN_ERR_MSG_UNDO",
  });
};

/* --------------------------  2fa Code Disable -------------------------- */

// export const disableTwofa = () => (dispatch) => {
//   const token = localStorage.getItem("token");
//   // Headers
//   const config = {
//     headers: {
//       "Content-Type": "application/json",
//       Authorization: `Bearer ${token}`,
//     },
//   };

//   axios
//     .post(TWO_FA_CODE_DISABLE_URL, "", config)
//     .then((res) =>
//       dispatch({
//         type: DISABLE_2FACODE_SUCCESS,
//         payload: res.data,
//       })
//     )
//     .catch((err) => {
//       dispatch({
//         type: DISABLE_2FACODE_FAIL,
//         payload: err.data,
//       });
//       console.log(err);
//     });
// };

/* --------------------- Setup config/headers and token --------------------- */
// const tokenConfig = (getState) => {
//   // Get token from localstorage
//   const token = getState().auth.token;

//   // Headers
//   const config = {
//     headers: {
//       "Content-type": "application/json",
//       Authorization: `Bearer ${token}`,
//     },
//   };

//   // If token, add to headers
//   if (token) {
//     config.headers["Authorization"] = `Bearer ${token}`;
//   }

//   return config;
// };

/* ------------------------------- 2fa code login ------------------------------- */

// export const twoFaLogin = ({ email, password, twofa_code }) => (dispatch) => {
//   // Headers
//   const config = {
//     headers: {
//       "Content-Type": "application/json",
//     },
//   };

//   // Request body
//   const body = { email, password, twofa_code };

//   axios
//     .post(LOGIN_URL, body, config)
//     .then((res) => {
//       dispatch({
//         type: LOGIN_SUCCESS,
//         payload: res.data,
//       });
//       dispatch(loadUser);
//     })
//     .catch((err) => {
//       dispatch({
//         type: LOGIN_FAIL,
//         payload: err.data,
//       });
//       // console.log(err)
//     });
// };

/* ------------------------- Change Password ------------------------ */
// export const changeSystemPassoword = ({
//   old_password,
//   new_password,
//   new_password_confirmation,
// }) => (dispatch) => {
//   // Get token from localstorage
//   const token = localStorage.getItem("token");

//   // Headers
//   const config = {
//     headers: {
//       "Content-type": "application/json",
//       Authorization: `Bearer ${token}`,
//     },
//   };

//   const newBody = {
//     old_password,
//     new_password,
//     new_password_confirmation,
//   };

//   axios
//     .put(CHANGE_PASSWORD_URL, newBody, config)
//     .then((res) => {
//       dispatch({
//         type: PASSWORD_CHANGE_SUCCESS,
//         payload: res.data,
//       });
//     })
//     .catch((err) => {
//       dispatch({
//         type: PASSWORD_CHANGE_FAIL,
//         payload: err.data,
//       });
//       console.log(err);
//       console.log(newBody);
//     });
// };
/* ------------------------- Set Page After Login ------------------------ */
// export const setPageAfterLogin = ({ post_login_page }) => (dispatch) => {
//   // Get token from localstorage
//   const token = localStorage.getItem("token");

//   // Headers
//   const config = {
//     headers: {
//       "Content-type": "application/json",
//       Authorization: `Bearer ${token}`,
//     },
//   };

//   const newBody = {
//     post_login_page,
//   };

//   axios
//     .put(PAGE_AFTER_LOGIN_URL, newBody, config)
//     .then((res) => {
//       dispatch({
//         type: SET_PAGE_AFTERLOGIN_SUCCESS,
//         payload: res.data,
//       });
//       dispatch(loadUser());
//     })
//     .catch((err) => {
//       dispatch({
//         type: SET_PAGE_AFTERLOGIN_FAIL,
//         payload: err.data,
//       });

//     });
// };

/* ------------------------------ Register User ----------------------------- */

// export const register = ({
//   username,
//   first_name,
//   last_name,
//   email,
//   password,
//   password_confirmation,
// }) => (dispatch) => {
//   // Headers
//   const config = {
//     headers: {
//       "Content-Type": "application/json",
//     },
//   };

//   // Request body
//   const body = {
//     username,
//     first_name,
//     last_name,
//     email,
//     password,
//     password_confirmation,
//   };

//   axios
//     .post(REGISTER_URL, body, config)
//     .then((res) =>
//       dispatch({
//         type: REGISTER_SUCCESS,
//         payload: res.data,
//       })
//     )
//     .catch((err) => {
//       dispatch({
//         type: REGISTER_FAIL,
//         payload: err.data,
//       });
//     });
// };

/* ------------------------- Forget password Request ------------------------ */

// export const requestPassword = ({ email }) => (dispatch) => {
//   // Headers
//   const config = {
//     headers: {
//       "Content-Type": "application/json",
//     },
//   };

//   // Request body
//   const body = {
//     email,
//   };

//   axios
//     .post(REQUEST_PASSWORD_URL, body, config)
//     .then((res) =>
//       dispatch({
//         type: RESET_PASSWOR_SUCCESS,
//         payload: res.data,
//       })
//     )
//     .catch((err) => {
//       dispatch({
//         type: RESET_PASSWOR_FAIL,
//         payload: err.data,
//       });
//     });
// };

/* ------------------------------- Logout User ------------------------------ */

export const logout = () => (dispatch) => {
  // Get token from localstorage
  const token = localStorage.getItem("token");

  // Headers
  const config = {
    headers: {
      "Content-type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  };

  axios
    .post(authCrud.LOGOUT_URL,"", config)
    .then((res) => {
      dispatch({
        type: "LOGOUT_SUCCESS",
      });
    })
    .catch((err) => {
      dispatch({
        type: "LOGOUT_FAIL",
      });
    });
};
