import axios from "axios";
import * as authTypes from "../../types/authTypes";

import * as authCrud from "../../cruds/authCruds";

export const forgotPassStepOne = ({ email }) => (dispatch) => {
  // Headers
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  // Request body
  const body = {
    email,
  };

  axios
    .post(authCrud.FORGOT_PASS_URL, body, config)
    .then((res) => {
      dispatch({
        type: authTypes.RESET_PASSWOR_STEP_ONE_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      if (
        err.response.data.message ===
        "We cant find a user with that e-mail address."
      ) {
        dispatch({
          type: authTypes.USER_DONT_EXIST,
        });
      }
      console.log(err.response.data.message);
    });
};
/* -------------------------- Reset Password Step2 -------------------------- */
export const forgotPassStepTwo = ({
  email,
  password,
  password_confirmation,
  token,
}) => (dispatch) => {
  // Headers
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  // Request body
  const body = {
    email,
    password,
    password_confirmation,
    token,
  };
  console.log(body);
  axios
    .post(authCrud.RESET_PASS_URL, body, config)
    .then((res) => {
      dispatch({
        type: authTypes.RESET_PASSWOR_SUCCESS,
        payload: res.data,
      });
      setTimeout(() => {
        dispatch({
          type: authTypes.RESET_PASSWOR_SUCCESS_UNDO,
        });
      }, 3000);
    })
    .catch((err) => {
      dispatch({
        type: authTypes.RESET_PASSWOR_FAIL,
      });

      console.log(err.response.data.message);
    });
};
