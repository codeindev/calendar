import axios from "axios";

import * as CRUD from "../../cruds/authCruds";


/* ------------------------- Get all Users ------------------------ */
export const fetchAllUsers = () => (dispatch) => {
  // Get token from localstorage
  const token = localStorage.getItem("token");

  // Headers
  const config = {
    headers: {
      "Content-type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  };
  axios
    .get(CRUD.GET_ALL_USERS_URL, config)
    .then((res) => {
      dispatch({
        type: "GET_ALL_USERS_LIST_SUCC",
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: "GET_ALL_USERS_LIST_ERR",
        payload: err.data,
      });
    });
};

/* ---------------------- Get Users Count ------------------------ */
export const getUsersCount = () => (dispatch) => {
  // Get token from localstorage
  const token = localStorage.getItem("token");

  // Headers
  const config = {
    headers: {
      "Content-type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  };
  axios
    .get(CRUD.GET_USERS_COUNT_URL, config)
    .then((res) => {
      dispatch({
        type: "USERS_COUNT_SUCC",
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: "USERS_COUNT_ERR",
        payload: err.data,
      });
    });
};

/* --------------------------- get users table row -------------------------- */

export const getUsersTableRowId = (data) => (dispatch) => {
  dispatch({
    type: "GET_USERS_TABLE_ROW_ID",
    payload: data,
  });
};

/* --------------------------- Show add User Modal -------------------------- */

export const showAddUserModal = () => (dispatch) => {
  dispatch({
    type: "SHOW_ADD_USER_MODAL",
  });
};

/* --------------------------- Hide Add User Modal -------------------------- */

export const hideAddUserModal = () => (dispatch) => {
  dispatch({
    type: "HIDE_ADD_USER_MODAL",
  });
};


/* ------------------------- Create NEW User ------------------------- */

export const createUser = ({
    user_name,
    first_name,
    last_name,
    mobile_number,
    email, 
    password, 
    password_confirmation
  }) => (dispatch) => {
    // Get token from localstorage
    const token = localStorage.getItem("token");
  
    // Headers
    const config = {
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
  
    const body = {
        user_name,
        first_name,
        last_name,
        mobile_number,
        email, 
        password, 
        password_confirmation
    };
  
    axios
      .post(CRUD.CREATE_USER_URL, body, config)
      .then((res) => {
        dispatch({
          type: "CREATE_USER_SUCC",
          payload: res.data,
        });
        setTimeout(() => {
          dispatch(undoCUSuccMsg());
        }, 2000);
      })
      .catch((err) => {
        dispatch({
          type: "CREATE_USER_ERR",
          payload: err.data,
        });
        console.log(err)
      });
  };

  
export const undoCUSuccMsg = () => (dispatch) => {
    dispatch({
      type: "CREATE_USER_SUCC_UNDO",
    });
  };


  
/* ------------------------------- Delete User ------------------------------ */

export const deleteUser = ( postTableRowID ) => (dispatch) => {
    // Get token from localstorage
    const token = localStorage.getItem("token");
  
    // Headers
    const config = {
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
  
  
    axios
      .delete(`${CRUD.DELETE_USER_URL}/${postTableRowID}`, config)
      .then((res) => {
        dispatch({
          type: "DELETE_USER_SUCC",
          payload: res.data,
        });
  
        dispatch(fetchAllUsers());
        setTimeout(() => {
          dispatch(undoUserDeleteSuccMsg());
        }, 4000);
      })
      .catch((err) => {
        dispatch({
          type: "DELETE_USER_ERR",
          payload: err.data,
        });
      });
  };
  
  /* -------------------------- Undo User Delete MSG -------------------------- */
  export const undoUserDeleteSuccMsg = () => (dispatch) => {
    dispatch({
      type: "DELETE_USER_SUCC_UNDO",
    });
  };

  
/* ------------------------- Show User Delete Modal ------------------------ */
export const showUserDeleteModal = (data) => (dispatch) => {
    dispatch({
      type: "SHOW_USER_DELETE_MODAL",
      payload: data,
    });
  };
  
  /* -------------------------- Hide User Delete Modal -------------------------- */
  export const hideUserDeleteModal = () => (dispatch) => {
    dispatch({
      type: "HIDE_USER_DELETE_MODAL",
    });
  };
