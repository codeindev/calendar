import React from "react";
import Aside from "./Aside";
import Footer from "./Footer";
import Header from "./Header";
import TopBar from "./TopBar";

export default function Layout({ children }) {
  return (
    <>
      {/* begin::Main */}
      <div className="d-flex flex-column flex-root">
        {/* TopBar Content Component */}
        <TopBar />
        {/* Header Component */}
        <Header />
        {/* Aside Component */}
        <Aside />
        {/* Begin Content */}
        <div className="content_wrapper">
          <div className="content_page">
            <section>{children}</section>
          </div>
        </div>
        {/* End Content */}
        {/* Footer Component */}
        <Footer />
      </div>
    </>
  );
}
