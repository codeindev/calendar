import React from 'react'
import { FormattedMessage } from 'react-intl'

export default function AuthPage() {
    return (
        <>
            <h2> <FormattedMessage id="AUTH_PLEASE_LOGIN" /> </h2>
        </>
    )
}
